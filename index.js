
const form = document.getElementById('form')
const playlist = document.getElementById('playlist')
const colors = ['alert-primary', 'alert-secondary', 'alert-success', 'alert-danger', 'alert-warning', 'alert-info', 'alert-light', 'alert-dark']

form.addEventListener('submit', handleSubmit)

function handleSubmit(event) {
   event.preventDefault();
   const data = new FormData(form)
   const title = data.get('value')
   store.dispatch({
      type: 'ADD_SONG',
      data: {
         title
      }
   })
}

// DEFINICION DEL STATE
const initialState = [
   { title: 'Just Jesus' },
   { title: 'Let Go' },
   { title: 'I Could Sing Of Your Love Forever' }
]

const reducer = (state, action) => {
   switch (action.type) {
      case 'ADD_SONG':
         return [...state, action.data]
      default:
         return state
   }
}

// CREACION DEL STORE
const store = Redux.createStore(
   reducer,
   initialState,
   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)


// SE OBTIENE DATOS DEL STORE
const state = store.getState()
addElement(state, playlist)

function handleChange() {
   const state = store.getState()
   addElement(state, playlist)
}

store.subscribe(handleChange)














function getRandomInt(max) {
   return Math.floor(Math.random() * Math.floor(max));
}

function addElement(state, playlist) {
   playlist.innerHTML = ''
   state.forEach(element => {
      const template = document.createElement('p')
      template.textContent = element.title
      template.classList.add('alert', 'alert-primary')
      playlist.appendChild(template)
   });

   const songs = document.getElementsByClassName('alert')

   for (const element of songs) {
      const num = getRandomInt(colors.length)
      element.classList.add(colors[num])
   }
}